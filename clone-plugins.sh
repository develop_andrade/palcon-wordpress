#!/bin/bash

OWN_REPOS="dodds2.0 wcms_racecards wcms-credit-card-support wcms_sportradar"
OTHER_REPOS="external_plugins"
REPO_BASE="git@bitbucket.org-luismolinaxlm:webpals/"

# Clone the base plugins wcms_plugins
# this repo becomes our plugins/ WP directory
git clone "${REPO_BASE}wcms_plugins.git" plugins

#now clone own others 
for r in $OWN_REPOS; do
    git clone $REPO_BASE$r.git
    ln -s ../$r/  plugins/$r
done

#now clone others
for r in $OTHER_REPOS; do
    git clone $REPO_BASE$r.git
    # symlink all first level directories 
    # assume they only contain plugins, right ?
    for s in `find external_plugins/ -maxdepth 1 -type d -not -name .git -not -name $r`; do
        P=`basename $s`
        ln -s ../$r/$P/ plugins/$P;
   done
done
